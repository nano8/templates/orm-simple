<?php

namespace nano\examples\orm\simple\user;

use laylatichy\nano\modules\orm\Model;

class User extends Model {
    public ?string $name;

    protected bool $redis       = true;

    protected bool $meiliSearch = true;

    public function jsonSerialize(): array {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'source'     => $this->source,
        ];
    }
}