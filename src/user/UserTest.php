<?php

namespace nano\examples\orm\simple\user;

use function Pest\Faker\fake;

describe('controller', function (): void {
    describe('->insert()', function (): void {
        it('should insert user', function (): void {
            $name = fake()->name();

            $userId = UserController::insert($name);
            $user   = User::fetch()->byId($userId);

            expect($user)
                ->toBeInstanceOf(User::class)
                ->name->toBe($name);

            $user?->delete();
        });
    });

    describe('->update()', function (): void {
        it('should update user', function (): void {
            $name = fake()->name();

            $userId = UserController::insert($name);

            $nameNew = fake()->name();
            $user    = UserController::update($userId, $nameNew);

            expect($user)
                ->toBeInstanceOf(User::class)
                ->name->toBe($nameNew);

            $user?->delete();
        });

        it('should not update if id is invalid', function (): void {
            $name = fake()->name();

            $userId = UserController::insert($name);

            expect(UserController::update(0, fake()->name()))
                ->toBeNull()
                ->and($user = User::fetch()->byId($userId))
                ->toBeInstanceOf(User::class)
                ->name->toBe($name);

            $user?->delete();
        });
    });
});
