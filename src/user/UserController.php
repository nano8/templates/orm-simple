<?php

namespace nano\examples\orm\simple\user;

class UserController {
    public static function insert(?string $name): int {
        $user       = new User();
        $user->name = $name;

        return $user->insert();
    }

    public static function update(int $id, ?string $name): ?User {
        $user = User::fetch()->byId($id);

        if (!$user) {
            return null;
        }

        $user->name = $name;
        $user->update();

        return $user;
    }
}