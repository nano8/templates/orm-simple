<?php

namespace nano\examples\orm\simple\user;

use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\core\request\Request;
use laylatichy\nano\core\response\Response;

defineMiddleware('user_validation', function (Request $request): void {
    useValidator($request->post(), [
        'name' => 'nullable|string',
    ]);
});

useRouter()->get('/user', fn (Request $request): Response => useResponse()
    ->withJson([
        'response' => useRepository(User::class, $request),
    ]));

useRouter()->post('/user', fn (Request $request): Response => useResponse()
    ->withCode(HttpCode::CREATED)
    ->withJson([
        'response' => UserController::insert($request->post('name')),
    ]), ['user_validation']);

useRouter()->get('/user/{id}', fn (Request $request, int $id): Response => useResponse()
    ->withJson([
        'response' => User::fetch()->byId($id),
    ]));

useRouter()->put('/user/{id}', fn (Request $request, int $id): Response => useResponse()
    ->withJson([
        'response' => UserController::update($id, $request->post('name')),
    ]), ['user_validation']);

useRouter()->delete('/user/{id}', function (Request $request, int $id): Response {
    User::fetch()->byId($id)?->delete();

    return useResponse()->withJson([
        'response' => true,
    ]);
});

useRouter()->post('/user/search', fn (Request $request): Response => useResponse()
    ->withJson([
        'response' => User::fetch()->search($request->post('term')),
    ]), ['search']);
