<?php

namespace nano\examples\orm\simple\address;

use laylatichy\nano\modules\orm\Model;
use nano\examples\orm\simple\user\User;

class Address extends Model {
    public string $city;

    public User $user;

    protected bool $redis       = true;

    protected bool $meiliSearch = true;

    public function jsonSerialize(): array {
        return [
            'id'         => $this->id,
            'city'       => $this->city,
            'user'       => $this->user,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'source'     => $this->source,
        ];
    }
}