<?php

namespace nano\examples\orm\simple\address;

use nano\examples\orm\simple\user\User;

class AddressController {
    public static function insert(string $city, User $user): int {
        $address       = new Address();
        $address->city = $city;
        $address->user = $user;

        return $address->insert();
    }

    public static function update(int $id, string $city, User $user): ?Address {
        $address = Address::fetch()->byId($id);

        if (!$address) {
            return null;
        }

        $address->city = $city;
        $address->user = $user;
        $address->update();

        return $address;
    }
}