<?php

namespace nano\examples\orm\simple\address;

use nano\examples\orm\simple\user\User;
use nano\examples\orm\simple\user\UserController;

use function Pest\Faker\fake;

describe('controller', function (): void {
    describe('->insert()', function (): void {
        it('should insert address', function (): void {
            $name = fake()->name();
            $city = fake()->city();

            $userId = UserController::insert($name);
            $user   = User::fetch()->byId($userId);

            $addressId = AddressController::insert($city, $user);
            $address   = Address::fetch()->byId($addressId);

            expect($address)
                ->toBeInstanceOf(Address::class)
                ->city->toBe($city)
                ->user->toBeInstanceOf(User::class)
                ->user->name->toBe($name);

            $user?->delete();
        });
    });

    describe('->update()', function (): void {
        it('should update address', function (): void {
            $name = fake()->name();
            $city = fake()->city();

            $userId = UserController::insert($name);
            $user   = User::fetch()->byId($userId);

            $addressId = AddressController::insert($city, $user);

            $cityNew = fake()->city();
            $address = AddressController::update($addressId, $cityNew, $user);

            expect($address)
                ->toBeInstanceOf(Address::class)
                ->city->toBe($cityNew)
                ->user->toBeInstanceOf(User::class)
                ->user->name->toBe($name);

            $user?->delete();
        });

        it('should not update address if id is invalid', function (): void {
            $name = fake()->name();
            $city = fake()->city();

            $userId = UserController::insert($name);
            $user   = User::fetch()->byId($userId);

            $addressId = AddressController::insert($city, $user);

            expect(AddressController::update(0, fake()->city(), $user))
                ->toBeNull()
                ->and(Address::fetch()->byId($addressId))
                ->toBeInstanceOf(Address::class)
                ->city->toBe($city)
                ->user->toBeInstanceOf(User::class)
                ->user->name->toBe($name);

            $user?->delete();
        });
    });
});
