<?php

namespace nano\examples\orm\simple\address;

use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\core\request\Request;
use laylatichy\nano\core\response\Response;
use nano\examples\orm\simple\user\User;

defineMiddleware('address_validation', function (Request $request): void {
    useValidator($request->post(), [
        'city'    => 'required|string',
        'user_id' => 'required|integer',
    ]);

    $user = User::fetch()->byId($request->post('user_id')); // @phpstan-ignore-line

    if (!$user) {
        useBadRequestException(['user not found']);
    }

    $request->context->set('user', $user);
});

useRouter()->get('/address', fn (Request $request): Response => useResponse()
    ->withJson([
        'response' => useRepository(Address::class, $request),
    ]));

useRouter()->post('/address', fn (Request $request): Response => useResponse()
    ->withCode(HttpCode::CREATED)
    ->withJson([
        'response' => AddressController::insert(
            $request->post('city'),
            $request->context->get('user'),
        ),
    ]), ['address_validation']);

useRouter()->get('/address/{id}', fn (Request $request, int $id): Response => useResponse()
    ->withJson([
        'response' => Address::fetch()->byId($id),
    ]));

useRouter()->put('/address/{id}', fn (Request $request, int $id): Response => useResponse()
    ->withJson([
        'response' => AddressController::update(
            $id,
            $request->post('city'),
            $request->context->get('user'),
        ),
    ]), ['address_validation']);

useRouter()->delete('/address/{id}', function (Request $request, int $id): Response {
    Address::fetch()->byId($id)?->delete();

    return useResponse()->withJson([
        'response' => true,
    ]);
});

useRouter()->post('/address/search', fn (Request $request): Response => useResponse()
    ->withJson([
        'response' => Address::fetch()->search($request->post('term')),
    ]), ['search']);