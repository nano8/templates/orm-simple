<?php

namespace tests\api;

use ApiTester;
use laylatichy\nano\core\httpcode\HttpCode;

use function Pest\Faker\fake;

class UserCest {
    /**
     * @group user
     */
    public function allEmpty(ApiTester $I): void {
        $I->sendGetAsJson('/user');
        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'response' => [
                'data' => 'array',
                'prev' => 'boolean',
                'next' => 'boolean',
            ],
        ]);
        $I->seeResponseContainsJson([
            'response' => [
                'data' => [],
                'prev' => false,
                'next' => false,
            ],
        ]);
    }

    /**
     * @group user
     */
    public function allSuccess(ApiTester $I): void {
        [$id, $name] = $this->_createUser($I);

        $I->sendGetAsJson('/user');

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'response' => [
                'data' => 'array',
                'prev' => 'boolean',
                'next' => 'boolean',
            ],
        ]);
        $I->seeResponseContainsJson([
            'response' => [
                'data' => [
                    [
                        'id'   => $id,
                        'name' => $name,
                    ],
                ],
                'prev' => false,
                'next' => false,
            ],
        ]);

        $this->_deleteUser($I, $id);
    }

    /**
     * @group user
     */
    public function singleNoUser(ApiTester $I): void {
        $I->sendGetAsJson('/user/0');
        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => null,
        ]);
    }

    /**
     * @group user
     */
    public function singleSuccess(ApiTester $I): void {
        [$id, $name] = $this->_createUser($I);

        $I->sendGetAsJson("/user/{$id}");

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                'id'   => $id,
                'name' => $name,
            ],
        ]);

        $this->_deleteUser($I, $id);
    }

    /**
     * @group user
     */
    public function createInvalidData(ApiTester $I): void {
        $I->sendPostAsJson('/user', [
            'name' => 1,
        ]);

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                'name must be a string',
            ],
        ]);
    }

    /**
     * @group user
     */
    public function createSuccess(ApiTester $I): void {
        $name = fake()->name();

        $I->sendPostAsJson('/user', [
            'name' => $name,
        ]);

        $I->seeResponseCodeIs(HttpCode::CREATED->code());
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'response' => 'integer',
        ]);

        [$id] = $I->grabDataFromResponseByJsonPath('$.response');

        $this->_deleteUser($I, $id);
    }

    /**
     * @group user
     */
    public function updateNoUser(ApiTester $I): void {
        $I->sendPutAsJson('/user/0', [
            'name' => fake()->name(),
        ]);

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => null,
        ]);
    }

    /**
     * @group user
     */
    public function updateInvalidData(ApiTester $I): void {
        [$id] = $this->_createUser($I);

        $I->sendPutAsJson("/user/{$id}", [
            'name' => 1,
        ]);

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                'name must be a string',
            ],
        ]);

        $this->_deleteUser($I, $id);
    }

    /**
     * @group user
     */
    public function updateSuccess(ApiTester $I): void {
        [$id] = $this->_createUser($I);

        $name = fake()->name();

        $I->sendPutAsJson("/user/{$id}", [
            'name' => $name,
        ]);

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                'id'   => $id,
                'name' => $name,
            ],
        ]);

        $this->_deleteUser($I, $id);
    }

    /**
     * @group user
     */
    public function deleteNoUser(ApiTester $I): void {
        $I->sendDeleteAsJson('/user/0');
        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => true,
        ]);
    }

    /**
     * @group user
     */
    public function deleteSuccess(ApiTester $I): void {
        [$id] = $this->_createUser($I);

        $I->sendDeleteAsJson("/user/{$id}");
        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => true,
        ]);
    }

    /**
     * @group user
     */
    public function searchEmpty(ApiTester $I): void {
        $I->sendPostAsJson('/user/search', [
            'term' => fake()->name(),
        ]);

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [],
        ]);
    }

    /**
     * @group user
     */
    public function searchSuccess(ApiTester $I): void {
        [$id, $name] = $this->_createUser($I);

        sleep(1);

        $I->sendPostAsJson('/user/search', [
            'term' => $name,
        ]);

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                [
                    'id'   => $id,
                    'name' => $name,
                ],
            ],
        ]);

        $this->_deleteUser($I, $id);
    }

    protected function _createUser(ApiTester $I): array {
        $name = fake()->name();

        $I->sendPostAsJson('/user', [
            'name' => $name,
        ]);

        [$id] = $I->grabDataFromResponseByJsonPath('$.response');

        return [$id, $name];
    }

    protected function _deleteUser(ApiTester $I, int $id): void {
        $I->sendDeleteAsJson("/user/{$id}");
    }
}
