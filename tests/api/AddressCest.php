<?php

namespace tests\api;

use ApiTester;
use laylatichy\nano\core\httpcode\HttpCode;

use function Pest\Faker\fake;

class AddressCest {
    /**
     * @group address
     */
    public function allEmpty(ApiTester $I): void {
        $I->sendGetAsJson('/address');
        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'response' => [
                'data' => 'array',
                'prev' => 'boolean',
                'next' => 'boolean',
            ],
        ]);
        $I->seeResponseContainsJson([
            'response' => [
                'data' => [],
                'prev' => false,
                'next' => false,
            ],
        ]);
    }

    /**
     * @group address
     */
    public function allSuccess(ApiTester $I): void {
        [$id, $city, $userId] = $this->_createAddress($I);

        $I->sendGetAsJson('/address');

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'response' => [
                'data' => 'array',
                'prev' => 'boolean',
                'next' => 'boolean',
            ],
        ]);
        $I->seeResponseContainsJson([
            'response' => [
                'data' => [
                    [
                        'id'   => $id,
                        'city' => $city,
                        'user' => [
                            'id' => $userId,
                        ],
                    ],
                ],
                'prev' => false,
                'next' => false,
            ],
        ]);

        $this->_deleteAddress($I, $id);
        $this->_deleteUser($I, $userId);
    }

    /**
     * @group address
     */
    public function singleNoAddress(ApiTester $I): void {
        $I->sendGetAsJson('/address/0');
        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => null,
        ]);
    }

    /**
     * @group address
     */
    public function singleSuccess(ApiTester $I): void {
        [$id, $city, $userId] = $this->_createAddress($I);

        $I->sendGetAsJson("/address/{$id}");

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'response' => [
                'id'   => 'integer',
                'city' => 'string',
                'user' => [
                    'id' => 'integer',
                ],
            ],
        ]);
        $I->seeResponseContainsJson([
            'response' => [
                'id'   => $id,
                'city' => $city,
                'user' => [
                    'id' => $userId,
                ],
            ],
        ]);

        $this->_deleteAddress($I, $id);
        $this->_deleteUser($I, $userId);
    }

    /**
     * @group address
     */
    public function createInvalidDataMissingCity(ApiTester $I): void {
        $I->sendPostAsJson('/address', [
            'user_id' => 0,
        ]);

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                'city is required',
            ],
        ]);
    }

    /**
     * @group address
     */
    public function createInvalidDataNoUser(ApiTester $I): void {
        $I->sendPostAsJson('/address', [
            'city'    => fake()->city(),
            'user_id' => 0,
        ]);

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                'user not found',
            ],
        ]);
    }

    /**
     * @group address
     */
    public function createSuccess(ApiTester $I): void {
        [$id, $city, $userId] = $this->_createAddress($I);

        $I->seeResponseCodeIs(HttpCode::CREATED->code());
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'response' => 'integer',
        ]);

        $this->_deleteAddress($I, $id);
        $this->_deleteUser($I, $userId);
    }

    /**
     * @group address
     */
    public function updateNoAddress(ApiTester $I): void {
        [$id, $city, $userId] = $this->_createAddress($I);

        $I->sendPutAsJson('/address/0', [
            'city'    => fake()->city(),
            'user_id' => $userId,
        ]);

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => null,
        ]);

        $this->_deleteAddress($I, $id);
        $this->_deleteUser($I, $userId);
    }

    /**
     * @group address
     */
    public function updateInvalidDataMissingCity(ApiTester $I): void {
        [$id, $city, $userId] = $this->_createAddress($I);

        $I->sendPutAsJson("/address/{$id}", [
            'user_id' => $userId,
        ]);

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                'city is required',
            ],
        ]);

        $this->_deleteAddress($I, $id);
        $this->_deleteUser($I, $userId);
    }

    /**
     * @group address
     */
    public function updateInvalidDataNoUser(ApiTester $I): void {
        [$id, $city, $userId] = $this->_createAddress($I);

        $I->sendPutAsJson("/address/{$id}", [
            'city'    => fake()->city(),
            'user_id' => 0,
        ]);

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                'user not found',
            ],
        ]);

        $this->_deleteAddress($I, $id);
        $this->_deleteUser($I, $userId);
    }

    /**
     * @group address
     */
    public function updateSuccess(ApiTester $I): void {
        [$id, $city, $userId] = $this->_createAddress($I);

        $cityNew = fake()->city();

        $I->sendPutAsJson("/address/{$id}", [
            'city'    => $cityNew,
            'user_id' => $userId,
        ]);

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                'id'   => $id,
                'city' => $cityNew,
                'user' => [
                    'id' => $userId,
                ],
            ],
        ]);

        $this->_deleteAddress($I, $id);
        $this->_deleteUser($I, $userId);
    }

    /**
     * @group address
     */
    public function deleteNoAddress(ApiTester $I): void {
        $I->sendDeleteAsJson('/address/0');

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => true,
        ]);
    }

    /**
     * @group address
     */
    public function deleteSuccess(ApiTester $I): void {
        [$id, $city, $userId] = $this->_createAddress($I);

        $I->sendDeleteAsJson("/address/{$id}");

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => true,
        ]);

        $this->_deleteUser($I, $userId);
    }

    /**
     * @group address
     */
    public function searchEmpty(ApiTester $I): void {
        $I->sendGetAsJson('/address');
        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [],
        ]);
    }

    /**
     * @group address
     */
    public function searchSuccess(ApiTester $I): void {
        [$id, $city, $userId] = $this->_createAddress($I);

        sleep(1);

        $I->sendPostAsJson('/address/search', [
            'term' => $city,
        ]);

        $I->seeResponseCodeIs(HttpCode::OK->code());
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'response' => [
                [
                    'id'   => $id,
                    'city' => $city,
                    'user' => [
                        'id' => $userId,
                    ],
                ],
            ],
        ]);

        $this->_deleteAddress($I, $id);
        $this->_deleteUser($I, $userId);
    }

    protected function _createAddress(ApiTester $I): array {
        [$userId] = $this->_createUser($I);

        $city = fake()->city();

        $I->sendPostAsJson('/address', [
            'city'    => $city,
            'user_id' => $userId,
        ]);

        [$id] = $I->grabDataFromResponseByJsonPath('$.response');

        return [$id, $city, $userId];
    }

    protected function _deleteAddress(ApiTester $I, int $id): void {
        $I->sendDeleteAsJson("/address/{$id}");
    }

    protected function _createUser(ApiTester $I): array {
        $name = fake()->name();

        $I->sendPostAsJson('/user', [
            'name' => $name,
        ]);

        [$id] = $I->grabDataFromResponseByJsonPath('$.response');

        return [$id, $name];
    }

    protected function _deleteUser(ApiTester $I, int $id): void {
        $I->sendDeleteAsJson("/user/{$id}");
    }
}
