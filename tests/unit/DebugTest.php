<?php

it('should not have debug', function (string $function): void {
    expect($function)->not->toBeUsed();
})->with([
    'dd',
    'dump',
    'var_dump',
]);