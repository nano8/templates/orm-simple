<?php

use laylatichy\nano\core\config\Environment;
use laylatichy\nano\modules\orm\OrmModule;

uses()->beforeAll(function () {
    useNano()->destroy();

    useConfig()
        ->withMode(isset($_ENV['CI']) ? Environment::CI : Environment::PEST)
        ->withRoot(__DIR__ . '/../src');

    useNano()->withModule(new OrmModule());
})->in('../src/*');
