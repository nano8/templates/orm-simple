<?php

return [
    'paths'         => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds'      => '%%PHINX_CONFIG_DIR%%/db/seeds',
    ],
    'environments'  => [
        'default_migration_table' => 'phinxlog',
        'default_environment'     => 'dev',
        'dev'                     => [
            'adapter' => 'mysql',
            'host'    => '127.0.0.1',
            'name'    => 'orm-simple',
            'user'    => 'app',
            'pass'    => 'app',
            'port'    => '53306',
            'charset' => 'utf8mb4',
        ],
        'test'                    => [
            'adapter' => 'mysql',
            'host'    => '127.0.0.1',
            'name'    => 'orm-simple',
            'user'    => 'app',
            'pass'    => 'app',
            'port'    => '53307',
            'charset' => 'utf8mb4',
        ],
        'ci'                      => [
            'adapter' => 'mysql',
            'host'    => 'mysql',
            'name'    => 'orm-simple',
            'user'    => 'root',
            'pass'    => 'root',
            'port'    => '3306',
            'charset' => 'utf8mb4',
        ],
    ],
    'version_order' => 'creation',
];
