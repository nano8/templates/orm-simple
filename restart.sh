#!/bin/sh

# This script is used to restart the nano
docker exec orm-simple-nano php index.php restart

docker exec orm-simple-nano-test php index.php restart

echo "restarted nano"
