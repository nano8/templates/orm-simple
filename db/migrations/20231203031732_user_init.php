<?php

use Phinx\Db\Table;
use Phinx\Migration\AbstractMigration;

final class UserInit extends AbstractMigration {
    private Table $table;

    public function change(): void {
        $this->table = $this->table('user', [
            'id'          => false,
            'primary_key' => 'id',
        ]);

        $this->columns();
    }

    private function columns(): void {
        $this->table->addColumn('id', 'biginteger', [
            'identity' => true,
            'signed'   => false,
        ])->addColumn('name', 'string', [
            'limit' => 255,
            'null'  => true,
        ])->addColumn('created_at', 'datetime', [
            'null' => false,
        ])->addColumn('updated_at', 'datetime', [
            'null' => false,
        ])->create();
    }
}
