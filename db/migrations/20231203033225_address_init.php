<?php

use Phinx\Db\Table;
use Phinx\Migration\AbstractMigration;

final class AddressInit extends AbstractMigration {
    private Table $table;

    public function change(): void {
        $this->table = $this->table('address', [
            'id'          => false,
            'primary_key' => 'id',
        ]);

        $this->columns();
    }

    private function columns(): void {
        $this->table->addColumn('id', 'biginteger', [
            'identity' => true,
            'signed'   => false,
        ])->addColumn('city', 'string', [
            'limit' => 255,
            'null'  => false,
        ])->addColumn('user_id', 'biginteger', [
            'signed' => false,
            'null'   => false,
        ])->addColumn('created_at', 'datetime', [
            'null' => false,
        ])->addColumn('updated_at', 'datetime', [
            'null' => false,
        ])->addForeignKey('user_id', 'user', 'id', [
            'delete' => 'CASCADE',
            'update' => 'CASCADE',
        ])->create();
    }
}
