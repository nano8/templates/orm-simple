<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude(
        [
            'codeception',
            'vendor',
            'node_modules',
        ]
    )->in(__DIR__)->append([__DIR__ . '/php-cs-fixer']);

$rules = [
    '@PHPUnit60Migration:risky'                   => true,
    '@PhpCsFixer'                                 => true,
    '@PhpCsFixer:risky'                           => false,
    'header_comment'                              => false,
    'list_syntax'                                 => ['syntax' => 'short'],
    'align_multiline_comment'                     => true,
    'phpdoc_align'                                => ['align' => 'vertical'],
    'phpdoc_to_comment'                           => false,
    'array_indentation'                           => true,
    'array_syntax'                                => ['syntax' => 'short'],
    'binary_operator_spaces'                      => [
        'operators' => [
            '=>' => 'align',
            '='  => 'align',
        ],
    ],
    'blank_line_after_namespace'                  => true,
    'blank_line_after_opening_tag'                => true,
    'blank_line_before_statement'                 => true,
    'cast_spaces'                                 => ['space' => 'none'],
    'increment_style'                             => ['style' => 'post'],
    'class_attributes_separation'                 => true,
    'class_definition'                            => true,
    'combine_consecutive_issets'                  => true,
    'combine_consecutive_unsets'                  => true,
    'combine_nested_dirname'                      => false,
    'concat_space'                                => ['spacing' => 'one'],
    'declare_equal_normalize'                     => ['space' => 'single'],
    'doctrine_annotation_indentation'             => true,
    'elseif'                                      => true,
    'encoding'                                    => true,
    'explicit_string_variable'                    => true,
    'full_opening_tag'                            => true,
    'function_declaration'                        => true,
    'global_namespace_import'                     => [
        'import_classes'   => true,
        'import_constants' => true,
        'import_functions' => true,
    ],
    'include'                                     => true,
    'indentation_type'                            => true,
    'line_ending'                                 => false,
    'linebreak_after_opening_tag'                 => false,
    'logical_operators'                           => true,
    'lowercase_cast'                              => true,
    'constant_case'                               => [
        'case' => 'lower',
    ],
    'lowercase_keywords'                          => true,
    'lowercase_static_reference'                  => true,
    'magic_method_casing'                         => true,
    'method_chaining_indentation'                 => false,
    'multiline_whitespace_before_semicolons'      => true,
    'native_function_casing'                      => true,
    'native_function_type_declaration_casing'     => true,
    'no_empty_comment'                            => true,
    'no_empty_statement'                          => true,
    'no_extra_blank_lines'                        => true,
    'no_multiline_whitespace_around_double_arrow' => true,
    'echo_tag_syntax'                             => [
        'format' => 'long',
    ],
    'no_singleline_whitespace_before_semicolons'  => true,
    'no_trailing_comma_in_singleline'             => true,
    'no_trailing_whitespace'                      => true,
    'no_trailing_whitespace_in_comment'           => true,
    'semicolon_after_instruction'                 => true,
    'short_scalar_cast'                           => true,
    'simple_to_complex_string_variable'           => true,
    'single_import_per_statement'                 => true,
    'ternary_operator_spaces'                     => true,
    'ternary_to_null_coalescing'                  => true,
    'trailing_comma_in_multiline'                 => [
        'elements' => ['arrays'],
    ],
    'trim_array_spaces'                           => true,
    'normalize_index_brace'                       => true,
    'single_line_after_imports'                   => false,
    'multiline_comment_opening_closing'           => false,
    'yoda_style'                                  => false,
    'no_blank_lines_after_phpdoc'                 => false,
    'no_whitespace_in_blank_line'                 => false,
    'single_blank_line_at_eof'                    => false,
    'single_space_around_construct'               => true,
    'control_structure_braces'                    => true,
    'control_structure_continuation_position'     => true,
    'declare_parentheses'                         => true,
    'no_multiple_statements_per_line'             => true,
    'curly_braces_position'                       => [
        'classes_opening_brace'   => 'same_line',
        'functions_opening_brace' => 'same_line',
    ],
    'no_extra_blank_lines'                        => true,
];

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules($rules)
    ->setFinder($finder);
