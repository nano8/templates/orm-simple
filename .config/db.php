<?php

defineDB(
    $_ENV['NANO_DB_HOST'],
    $_ENV['NANO_DB_PORT'],
    $_ENV['NANO_DB_NAME'],
    $_ENV['NANO_DB_USER'],
    $_ENV['NANO_DB_PASS'],
    $_ENV['NANO_DB_DRIVER'],
);

defineRedis(
    $_ENV['NANO_REDIS_HOST'],
    $_ENV['NANO_REDIS_PORT'],
    $_ENV['NANO_REDIS_PASS'],
);

defineMeiliSearch(
    $_ENV['NANO_MEILISEARCH_HOST'],
    $_ENV['NANO_MEILISEARCH_PORT'],
    $_ENV['NANO_MEILISEARCH_PASS'],
);