<?php

use laylatichy\nano\core\config\Environment;
use laylatichy\nano\core\request\Request;
use laylatichy\nano\core\response\Response;
use laylatichy\nano\modules\orm\OrmModule;
use laylatichy\nano\modules\validator\ValidatorModule;

require_once __DIR__ . '/../vendor/autoload.php';

useConfig()
    ->withMode(Environment::from(getenv('MODE')))
    ->withRoot(__DIR__ . '/../src');

useNano()
    ->withoutCache()
    ->withHeader('content-type', 'application/json')
    ->withModule(new ValidatorModule())
    ->withModule(new OrmModule());

defineMiddleware('search', function (Request $request): void {
    useValidator($request->post(), [
        'term' => 'required|string',
    ]);
});

useRouter()->get('/', fn (): Response => useStatusResponse());

useNano()->start();
